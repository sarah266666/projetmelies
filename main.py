# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 00:06:38 2018

@author: Megane Laurent
"""

import sqlite3
import tkinter
from AgroPythia_v2 import *
from agro_GUI.agro_widgets import *

def connexion_base_de_donnees() :
    """
    Déterminer une routine de connexion à la base de données spectacles.sqlite située dans le dossier bdd
    """
    #bloc_instructions
    "Chemin relatif de la base de données"
    chemin_bdd = "bdd/spectacles.sqlite"
    
    "Connexion à la base de données"
    try :
        connexion = sqlite3.connect(chemin_bdd)
        return connexion
    except Error as e :
        print(e)
        
    return None

def acces_accueil() :
    """
    Accéder à la fenêtre d'accueil
    """
    #bloc_instructions
    construire_contenu_fenetre("accueil")
    
def acces_consultation() :
    """
    Accéder à la fenêtre de consultation
    """
    #bloc_instructions
    construire_contenu_fenetre("consultation")
    
def acces_modification() :
    """
    Accéder à la fenêtre de modification
    """
    #bloc_instructions
    construire_contenu_fenetre("modification")
    
def acces_ajout() :
    """
    Accéder à la fenêtre d'ajout
    """
    #bloc_instructions
    construire_contenu_fenetre("ajout")
    
def acces_suppression() :
    """
    Accéder à la fenêtre de suppression
    """
    #bloc_instructions
    construire_contenu_fenetre("suppression")

def construire_contenu_fenetre(rubrique) :
    """
    Construction du contenu à afficher en fonction de la rubrique demandée
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.grid()
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de la page d'accueil"
    if(rubrique == "accueil") :
        fenetre_application.inserer_titre("Accueil", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Consultation, Modification, Ajout et Suppression.")
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de consultation"
    if(rubrique == "consultation") :
        liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
        liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
        fenetre_application.inserer_titre("Consultation", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
        fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de modification"
    if(rubrique == "modification") :
        liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
        liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
        fenetre_application.inserer_titre("Modification", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
        fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page d'ajout"
    if(rubrique == "ajout") :
        liste_labels_ajout = ["Ajouter un spectacle", "Ajouter une représentation", "Ajouter un client"]
        liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
        fenetre_application.inserer_titre("Ajout", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter un spectacle, ajouter une représentation, ajouter un client.")
        fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de suppression"
    if(rubrique == "suppression") :
        liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Clients"]
        liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
        fenetre_application.inserer_titre("Suppression", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
        fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "S'assurer que l'affichage des fenêtres soit correcte"
    fenetre_application.manage_view()
    
    return fenetre_application

def afficher_fenetre(contenu_fenetre) :
    """
    Déterminer une routine d'affichage d'une fenêtre de l'application
    """
    #bloc_instructions
    contenu_fenetre.show()
    
def construire_liste_boutons() :
    """
    Construction d'une liste de boutons standard pour toutes les pages
    """
    #bloc_instructions
    liste_labels = ["Accueil", "Consultation", "Modification", "Ajout", "Suppression"]
    liste_actions = [acces_accueil, acces_consultation, acces_modification, acces_ajout, acces_suppression]
    liste_boutons = [liste_labels, liste_actions]
    
    return liste_boutons

def recuperer_resultat_requete(stockage_de_requete):
    """
    Déterminer une routine d'affichage d'une requête SQL
    """
    #bloc_instructions
    liste = stockage_de_requete.fetchall()
    
    return liste

def consulter_fonctionnalite(fonctionnalite, offset="0") :
    """
    Déterminer une routine de consultation de table pour chaque fonctionnalité. Le paramètre offset permet de créer des pages limitées en nombre d'information.
    """
    #bloc_instructions
    "Lister les caractéristiques des spectacles"
    if(fonctionnalite == "lister_les_caracteristiques_des_spectacles") :
        requete = "SELECT libelle, site_web, date, heure FROM spectacle AS s, representation AS r WHERE r.id_spectacle = s.id ORDER BY s.id LIMIT 20 OFFSET " + str(offset)
    
    "Lister les places disponibles"
    if(fonctionnalite == "lister_les_places_disponibles") :
        requete = "SELECT libelle, date, heure, type_place, prix, nb_places FROM spectacle AS s, representation AS r, place AS p WHERE p.id_representation = r.id AND r.id_spectacle = s.id ORDER BY s.id LIMIT 20 OFFSET " + str(offset)
    
    "Lister les réservations des clients"
    if(fonctionnalite == "quel_client_a_reserve_quoi") :
        requete = "SELECT nom, prenom, resa.nb_places, date_reservation, libelle, heure, date, prix FROM client AS c, reservation AS resa, place AS p, representation AS r, spectacle AS s WHERE resa.id_client = c.id AND resa.id_place = p.id AND p.id_representation = r.id AND r.id_spectacle = s.id ORDER BY c.id LIMIT 20 " + "OFFSET " + str(offset)
    
    "Lister les représentations en vue d'une modification"
    if(fonctionnalite == "modifier_une_representation") :
        requete = "SELECT r.id, s.libelle, r.date, r.heure FROM spectacle AS s, representation as r WHERE s.id = r.id_spectacle"
        
    "Lister les places en vue d'une modification"
    if(fonctionnalite == "modifier_une_place") :
        requete = "SELECT p.id, s.libelle, r.date, r.heure, p.type_place, p.prix FROM spectacle AS s, representation AS r, place AS p WHERE s.id = r.id_spectacle AND r.id = p.id_representation ORDER BY p.id"
    
    "Lister les clients en vue d'une modification"
    if(fonctionnalite == "modifier_un_client") :
        requete = "SELECT * FROM client ORDER BY id"
    
    "Lister les spectacles en vue d'en ajouter"
    if(fonctionnalite == "ajouter_un_spectacle") :
        requete = "SELECT * FROM spectacle ORDER BY id DESC LIMIT 5"
    
    "Lister les représentations en vue d'en ajouter"
    if(fonctionnalite == "lister_representation") :
        requete = "SELECT r.id, r.date, r.heure, s.libelle FROM spectacle AS s, representation AS r WHERE s.id = r.id_spectacle ORDER BY r.id DESC LIMIT 5"
    
    "Lister les spectacles pour savoir laquelle ajouter en représentation"
    if(fonctionnalite == "ajouter_une_representation") :
        requete = "SELECT id, libelle FROM spectacle ORDER BY libelle"
    
    "Lister les clients en vue d'en ajouter"
    if(fonctionnalite == "ajouter_un_client") :
        requete = "SELECT * FROM client ORDER BY id DESC LIMIT 5"
    
    "Lister les représentations en vue d'en supprimer"
    if(fonctionnalite == "supprimer_une_representation") :
        requete = "SELECT r.id, s.libelle, r.date, r.heure FROM spectacle AS s, representation AS r WHERE r.id_spectacle = s.id ORDER BY r.id"
    
    "Lister les réservations en vue d'en supprimer"
    if(fonctionnalite == "supprimer_une_reservation") :
        requete = "SELECT resa.id, c.nom, c.prenom, resa.nb_places, p.prix, r.date, r.heure, s.libelle FROM spectacle AS s, representation AS r, place AS p, reservation AS resa, client AS c WHERE s.id = r.id_spectacle AND r.id = p.id_representation AND p.id = resa.id_place AND resa.id_client = c.id"
    
    "Lister les clients en vue d'en supprimer"
    if(fonctionnalite == "supprimer_un_client") :
        requete = "SELECT * FROM client ORDER BY id"
    
    return requete

def consulter_spectacles() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    global offset_consulter_spectacles
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    offset_consulter_spectacles = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
                                           
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_spectacles_precedent() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_spectacles
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")

    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Prévoir un offset de 20 éléments en moins pour la page précédentes"
    offset_consulter_spectacles = offset_consulter_spectacles - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_spectacles < 0) :
        offset_consulter_spectacles = 0
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()

def consulter_spectacles_suivant() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_spectacles
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_spectacles = offset_consulter_spectacles + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    offset_consulter_places = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places_precedent() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_places = offset_consulter_places - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_places < 0) :
        offset_consulter_places = 0
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places_suivant() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_places = offset_consulter_places + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    offset_consulter_clients = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients_precedent() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_clients = offset_consulter_clients - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_clients < 0) :
        offset_consulter_clients = 0
        
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients_suivant() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_clients = offset_consulter_clients + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_representation() :
    """
    Affiche les représentations pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des représentations"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une représentation, indiquer la nouvelle date et heure et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_calendrier("representation", "Nouvelle date de la représentation sélectionnée")
    fenetre_application.inserer_champ_text("heure", "Nouveaux horaires de la représentation sélectionnée sous format HH:MM:SS")
    fenetre_application.inserer_bouton("Modifier", modifier_representation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_representation_selectionnee() :
    """
    Modifier la représentation sélectionnée
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element[1])
    
    "Récupération des données utiles"
    id_representation = contenu[0][0][0]
    date_representation = contenu[1]
    heure_representation = contenu[2]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modification de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE representation SET date = ?, heure = ? WHERE id = ?", (date_representation, heure_representation, id_representation,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des représentations"
    modifier_representation()
    
def modifier_places() :
    """
    Affiche les places pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des places"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier places", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_une_place"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une place, indiquer le nouveau prix en € et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_champ_text("prix", "Nouveau prix au format XXX")
    fenetre_application.inserer_bouton("Modifier", modifier_place_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_place_selectionnee() :
    """
    Modifier le prix d'une place sélectionnée
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_place = contenu[0][1][0][0]
    prix_place = contenu[1][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modifier de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE place SET prix = ? WHERE id = ?", (prix_place, id_place,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des places"
    modifier_places()
    
def modifier_clients() :
    """
    Affiche les clients pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des clients"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner un client, remplir son nom, prénom, email et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_champ_text("nom", "Nouveau Nom")
    fenetre_application.inserer_champ_text("prenom", "Nouveau Prénom")
    fenetre_application.inserer_champ_text("email", "Nouveau Mail")
    fenetre_application.inserer_bouton("Modifier", modifier_client_selectionne)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_client_selectionne() :
    """
    Modifier les coordonnées du client sélectionné
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_client = contenu[0][1][0][0]
    nom_actuel_client = contenu[0][1][0][1]
    prenom_actuel_client = contenu[0][1][0][2]
    email_actuel_client = contenu[0][1][0][3]
    
    nouveau_nom_client = contenu[1][1]
    nouveau_prenom_client = contenu[2][1]
    nouvel_email_client = contenu[3][1]
    
    "On prévoit certaines cases vides"
    if(len(nouveau_nom_client) == 0) :
        nouveau_nom_client = nom_actuel_client
    if(len(nouveau_prenom_client) == 0) :
        nouveau_prenom_client = prenom_actuel_client
    if(len(nouvel_email_client) == 0) :
        nouvel_email_client = email_actuel_client
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modifier de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE client SET prenom = ?, nom = ?, email = ? WHERE id = ?", (nouveau_nom_client, nouveau_prenom_client, nouvel_email_client, id_client,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des clients"
    modifier_clients()
    
def ajouter_spectacle() :
    """
    Affiche un formulaire pour ajouter un spectacle
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout de spectacles"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter un spectacle", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_un_spectacle"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Libellé", "Site Web"]
    
    "Afficher les spectacles existants"
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    fenetre_application.inserer_champ_text("libelle", "Libellé")
    fenetre_application.inserer_champ_text("site", "Site Web")
    fenetre_application.inserer_bouton("Ajouter", ajouter_spectacle_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_spectacle_formulaire() :
    """
    Ajouter le nouveau spectacle
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    libelle_spectacle = contenu[0][1]
    site_spectacle = contenu[1][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    stockage_de_requete.execute("INSERT INTO spectacle (libelle, site_web) VALUES (?, ?)", (libelle_spectacle, site_spectacle,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout de spectacles"
    ajouter_spectacle()
    
def ajouter_representation() :
    """
    Affiche un formulaire pour ajouter une représentation
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout des représentation"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Date", "Heure", "Libellé"]
    
    "Afficher les représentations existantes"
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    
    "Afficher les spectacles existants"
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    fenetre_application.inserer_listebox("liste_spectacle", "Sélectionner le spectacle qui sera représenté", liste_valeurs)
    
    fenetre_application.inserer_calendrier("date", "Date de la nouvelle représentation")
    fenetre_application.inserer_champ_text("heure", "Heure de la nouvelle représentation au format HH:MM:SS")
    fenetre_application.inserer_bouton("Ajouter", ajouter_representation_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_representation_formulaire() :
    """
    Ajouter la nouvelle représentation
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_spectacle = contenu[0][1][0][0]
    date_representation = contenu[1][1]
    heure_representation = contenu[2][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("INSERT INTO representation (id_spectacle, date, heure) VALUES (?, ?, ?)", (id_spectacle, date_representation, heure_representation,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout des représentations"
    ajouter_representation()
    
def ajouter_client() :
    """
    Affiche un formulaire pour ajouter un client
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout des clients"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter client", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Nom", "Prénom", "Email"]
    
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    fenetre_application.inserer_champ_text("nom", "Nom du nouveau client")
    fenetre_application.inserer_champ_text("prenom", "Prénom du nouveau client")
    fenetre_application.inserer_champ_text("email", "Email du nouveau client")
    fenetre_application.inserer_bouton("Ajouter", ajouter_client_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_client_formulaire() :
    """
    Ajouter le nouveau client
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    nom_client = contenu[0][1]
    prenom_client = contenu[1][1]
    email_client = contenu[2][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("INSERT INTO client (nom, prenom, email) VALUES (?, ?, ?)", (nom_client, prenom_client, email_client,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout des clients"
    ajouter_client()
    
def supprimer_representation() :
    """
    Supprimer une représentation
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression d'une représentation"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer une représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une représentation et appuyer sur Valider pour la supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_representation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_representation_selectionnee() :
    """
    Supprimer une représentation sélectionnée dans le menu déroulant en s'assurant d'avoir supprimé sa les places et les réservations au préalable afin de satisfaire les contraintes de clés étrangères
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_representation_a_supprimer = str(element[1][0][0])
        
    "Il faut au préalable supprimer les réservations en lien avec les places qui sont en lien avec la représentation"
    "On liste toutes les places ayant pour clé étrangère la représentation qu'on souhaite supprimer"
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Quand une réservation existe, on supprime d'abord la réservation"
    try :
        stockage_de_requete.execute("SELECT id FROM place WHERE id_representation = ?", id_representation_a_supprimer)
        
        for id_place in stockage_de_requete.fetchall() :
            stockage_de_requete.execute("DELETE FROM reservation WHERE id_place = ?", (str(id_place[0]),))
        
        "Valider la requête de suppression des réservations"
        connexion.commit()
        
        "On supprime les places en rapport avec les réservations précédentes"
        stockage_de_requete.execute("SELECT id FROM place WHERE id_representation = ?", id_representation_a_supprimer)
        for id_place in stockage_de_requete.fetchall() :
            stockage_de_requete.execute("DELETE FROM place WHERE id_representation = ?", (id_representation_a_supprimer,))
            
        "Valider la requête de suppression des places"
        connexion.commit()
        
        "On supprimer les réservations en rapport avec les places précédentes"
        stockage_de_requete.execute("DELETE FROM representation WHERE id = ?", (id_representation_a_supprimer,))
            
        "Valider la requête de suppression des places"
        connexion.commit()
    except :
        "Quand aucune réservation n'existe, on supprime directement les places puis la représentation"
        stockage_de_requete.execute("DELETE FROM place WHERE id_representation = ?", (id_representation_a_supprimer,))
        stockage_de_requete.execute("DELETE FROM representation WHERE id = ?", (id_representation_a_supprimer,))
        connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des représentations"
    supprimer_representation()
    
def supprimer_reservation() :
    """
    Supprimer une réservation
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression d'une représentation"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer une réservation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_une_reservation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une réservation et appuyer sur Supprimer pour la supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_reservation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_reservation_selectionnee() :
    """
    Supprimer la réservation sélectionnée du menu déroulant
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_reservation_a_supprimer = str(element[1][0][0])
        
    "Connexion à la base de données"    
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("DELETE FROM reservation WHERE id = ?", (id_reservation_a_supprimer,))
    
    "Valider la requête"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des réservations"
    supprimer_reservation()
    
def supprimer_client() :
    """
    Supprimer un client sélectionné dans le menu déroulant en s'assurant d'avoir supprimé sa réservation au préalable afin de satisfaire la contrainte de clé étrangère
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression des clients"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer un client", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à une base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_client", "Sélectionner un client et appuyer sur Supprimer pour le supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_client_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_client_selectionnee() :
    """
    Supprimer le client sélectionné du menu déroulant
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_client_a_supprimer = str(element[1][0][0])
        
    "Connexion à la base de données"    
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Suppression des réservations effectuées par le client avant de supprimer le client de la liste"
    stockage_de_requete.execute("DELETE FROM reservation WHERE id_client = ?", (id_client_a_supprimer,))
    stockage_de_requete.execute("DELETE FROM client WHERE id = ?", (id_client_a_supprimer,))
    
    "Valider la requête"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des clients"
    supprimer_client()
    
def lanceur_application() :
    """
    Déterminer une routine de lancement d'application
    """
    #bloc_instructions
    afficher_fenetre(construire_contenu_fenetre("accueil"))

"""
Exécution du programme
"""
fenetre_application = Agro_App("Méliès Manager")

"Offset de la requête SQL"
offset_consulter_spectacles = 0
offset_consulter_places = 0
offset_consulter_clients = 0
lanceur_application()