# -*- coding: utf-8 -*-
"""
Utilisation de la base de données client_commande acceptant les requêtes SQL
"""
from __future__ import unicode_literals
import sqlite3
import tkinter as tk
import time
#import tkFont
from tkinter import messagebox as tkMessageBox
from tkinter import filedialog as tkFileDialog
#import sys
import bd_agro_GUI as bdGUI

import os
#import ttk

  

def file_is_empty(path):
    return os.stat(path).st_size==0


def OnHsb(*args):
    fenetre.list_results.xview(*args)
    fenetre.list_champs.xview(*args)
    
def new_req():
    fenetre.results_frame.destroy()
    fenetre.requete_frame.pack(padx=5, pady=5, fill=tk.BOTH, expand=True)   

def affiche_modifs(cur,requete, tk_GUI):#, log_file) :
    
    r = cur.fetchone()
    j=1
    if r : #Si on a des données correspondant à la requête
        tk.Label(fenetre.results_frame, text= u"Voici ce qu'elle contiendra si vous validez cette action :",font = "Verdana 10").pack(anchor=tk.W, pady=8)            
                
        fr = tk.LabelFrame(tk_GUI,text='', bd=0)
        fr.pack( fill=tk.BOTH, expand=True, padx=4, pady=10) #side=tk.LEFT,
    
        #On crée la liste qui permettra de les afficher 
        fenetre.list_results = tk.Listbox(fr, font='courier 10', height=5)
        fenetre.list_champs = tk.Listbox(fr, font='courier 10 bold', height=1)
        scroll_v = tk.Scrollbar(fr, orient=tk.VERTICAL, command=fenetre.list_results.yview)  
        scroll_h = tk.Scrollbar(fr, orient=tk.HORIZONTAL, command=OnHsb)         
        fenetre.list_results['xscrollcommand'] = scroll_h.set
        fenetre.list_results['yscrollcommand'] = scroll_v.set        
        
        #on récupère les noms des colonnes
        champs = r.keys()
    
        donnees = []
        for value in r :
            donnees.append([value])
        #onstocke les données pour la mise en page ultérieure   
        for enreg in cur: 	
            for i in range(len(enreg)) :
                donnees[i].append(enreg[i])    
        #on cherche par champs la chaine la plus longue
        lenghts = []
        for liste in donnees :
            temp = []
            for value in liste :
                #temp.append(len(unicode(value)))
                temp.append(len(str(value)))
            lenghts.append(max(temp))
        
        #affichage des intitulés des colonnes        
        ligne = ''
        ligne_log = ''
        for i in range(len(champs)) :
#            ligne += unicode(champs[i])+(lenghts[i]-len(champs[i])+8)*' '   
#            ligne_log += unicode(champs[i])+(lenghts[i]-len(champs[i])+4)*' '  
            ligne += champs[i]+(lenghts[i]-len(champs[i])+8)*' '   
            ligne_log += champs[i]+(lenghts[i]-len(champs[i])+4)*' '                
            
#        log_file.write(ligne_log+'\n')
#        log_file.write('-'*len(ligne_log)+'\n')
        fenetre.list_champs.pack(fill=tk.X)#, expand=True)
        fenetre.list_champs.insert('end',ligne)

        scroll_h.pack(side=tk.BOTTOM, fill=tk.X)#, expand=True)
        fenetre.list_results.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scroll_v.pack(fill=tk.Y, expand=True)

        #affichage des lignes de résultats    
        for i in range(len(donnees[0])) :
            ligne = ''
            ligne_log=''
            for j in range(len(donnees)) :
#                ligne += unicode(donnees[j][i])+(lenghts[j]-len(unicode(donnees[j][i]))+8)*' '
#                ligne_log += unicode(donnees[j][i])+(lenghts[j]-len(unicode(donnees[j][i]))+4)*' '
                ligne += str(donnees[j][i])+(lenghts[j]-len(str(donnees[j][i]))+8)*' '
                ligne_log += str(donnees[j][i])+(lenghts[j]-len(str(donnees[j][i]))+4)*' '            

            fenetre.list_results.insert('end', ligne)
            
            if ((i-1)%2) :
                fenetre.list_results.itemconfigure(i, background='cornsilk2') 
            #log_file.write(ligne_log.encode('utf-8'))
#        #    log_file.write(ligne_log)
#            log_file.write('\n') 
            
            fenetre.list_champs.config(state=tk.DISABLED)            
            fenetre.list_results.config(activestyle='none')
    else :
        tk.Label(tk_GUI, text=u"La table est vide !",font = "Verdana 10").pack(anchor=tk.W)
#        log_file.write(u"\nLa table est vide !")
#    log_file.flush()
    

def affiche_select(cur,requete, tk_GUI):#, log_file) :
    
    r = cur.fetchone()
    j=1
    if r : #Si on a des données correspondant à la requête
        label_fr = tk.Frame(fenetre.results_frame)
        label_fr.pack(fill=tk.X)
        tk.Label(label_fr, text=u"Voici les données extraites de la base de données suite à votre requête :",font = "Verdana 10").pack(side=tk.LEFT,anchor=tk.W, padx='8', pady='3')
        tk.Label(label_fr, text=requete,font = "courier 10",wraplength=600, justify='left').pack(anchor=tk.W, padx='8', pady='5')

        fr = tk.LabelFrame(tk_GUI,text='', bd=0)
        fr.pack(fill=tk.BOTH, expand=True, padx=4, pady=10)# side=tk.LEFT,
    
        #On crée la liste qui permettra de les afficher 
        fenetre.list_results = tk.Listbox(fr, font='courier 10', height=5)
        fenetre.list_champs = tk.Listbox(fr, font='courier 10 bold', height=1)
        scroll_v = tk.Scrollbar(fr, orient=tk.VERTICAL, command=fenetre.list_results.yview)  
        scroll_h = tk.Scrollbar(fr, orient=tk.HORIZONTAL, command=OnHsb)         
        fenetre.list_results['xscrollcommand'] = scroll_h.set
        fenetre.list_results['yscrollcommand'] = scroll_v.set        
        
        #on récupère les noms des colonnes
        champs = r.keys()
    
        donnees = []
        for value in r :
            donnees.append([value])
        #onstocke les données pour la mise en page ultérieure   
        for enreg in cur: 	
            for i in range(len(enreg)) :
                donnees[i].append(enreg[i])    
        #on cherche par champs la chaine la plus longue
        lenghts = []
        for liste in donnees :
            temp = []
            for value in liste :
                temp.append(len(str(value)))
            lenghts.append(max(temp))
        
        #affichage des intitulés des colonnes        
        ligne = ''
        ligne_log = ''
        for i in range(len(champs)) :
#            ligne += unicode(champs[i])+(lenghts[i]-len(champs[i])+8)*' ' 
#            ligne_log += unicode(champs[i])+(lenghts[i]-len(champs[i])+4)*' '
            ligne += champs[i]+(lenghts[i]-len(champs[i])+8)*' ' 
            ligne_log += champs[i]+(lenghts[i]-len(champs[i])+4)*' '        
            
#        log_file.write(ligne_log+'\n')
#        log_file.write('-'*len(ligne_log)+'\n')
        fenetre.list_champs.pack(fill=tk.X) #, expand=True)
        fenetre.list_champs.insert('end',ligne)

        scroll_h.pack(side=tk.BOTTOM, fill=tk.X)#, expand=True)
        fenetre.list_results.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scroll_v.pack(fill=tk.Y, expand=True)

        #affichage des lignes de résultats    
        for i in range(len(donnees[0])) :
            ligne = ''
            ligne_log=''
            for j in range(len(donnees)) :
                ligne += str(donnees[j][i])+(lenghts[j]-len(str(donnees[j][i]))+8)*' '
                ligne_log += str(donnees[j][i])+(lenghts[j]-len(str(donnees[j][i]))+4)*' '

            fenetre.list_results.insert('end', ligne)
            
            if ((i-1)%2) :
                fenetre.list_results.itemconfigure(i, background='cornsilk2') 
                
#            log_file.write(ligne_log+'\n')
        
        fenetre.list_champs.config(state=tk.DISABLED)            
        fenetre.list_results.config(activestyle='none')
            
    else :
        tk.Label(tk_GUI, text=u"\nIl n'y a pas de données correspondant à votre requête : ",font = "Verdana 11").pack(anchor=tk.W)
        tk.Label(tk_GUI, text=requete+u'\n',font = "courier 10").pack()#(anchor=tk.W)
        #requete = requete.encode('utf-8')
        mess=u"\nIl n'y a pas de données correspondant à votre requête :\n"+requete
        #mess = mess.encode('utf-8')
#        log_file.write(mess)
    
        #new_req_Button=tk.Button(tk_GUI, text=u'Nouvelle requête', command=new_req)
        #new_req_Button.pack(side=tk.BOTTOM, padx=10, pady=10)#, anchor = tk.E   
    new_req_Button=tk.Button(tk_GUI, text=u'Nouvelle requête', command=new_req)
    new_req_Button.pack(side=tk.BOTTOM, padx=10, pady=10)#, anchor = tk.E    
#    log_file.flush()

def valider_modif():
    fenetre.baseDonn.commit()
    #fenetre.valid_GUI = tk.Tk()
    #fenetre.valid_GUI.title("Validation")
    #tk.Label(fenetre.valid_GUI, text="\nLa base de données a été mise à jour.").pack()
    mess = u'Votre modification est prise en compte'
    tkMessageBox.showinfo("",mess)
    clear_fenetre()
    compose_fenetre()
    #print fenetre.modif_table
    activate_tab(fenetre.modif_table)
    #fenetre.log_file.write(u'La modification a été validée ! \n'.encode('utf-8'))
#    fenetre.log_file.write(u'La modification a été validée ! \n')
#    fenetre.log_file.flush()

def abandonner_modif():
    fenetre.baseDonn.rollback()
    mess = u'Votre modification a été annulée'
    tkMessageBox.showinfo("",mess)
    clear_fenetre()
    compose_fenetre()
    activate_tab(fenetre.modif_table)
    #fenetre.log_file.write(u'La modification a été annulée ! \n'.encode('utf-8'))
#    fenetre.log_file.write(u'La modification a été annulée ! \n')
#    fenetre.log_file.flush()
    
def exec_req(requete):

    try:
    
        sav = fenetre.baseDonn.row_factory  # sauvegarde de l'ancienne valeur (None par défaut)
        fenetre.baseDonn.row_factory = sqlite3.Row
        cur = fenetre.baseDonn.cursor()
        fenetre.baseDonn.row_factory = sav # retour à l'ancienne valeur
        cur.execute(requete)	   # exécution de la requête SQL 

    except sqlite3.Error as e :
        fenetre.results_frame.destroy() 
        fenetre.requete_frame.pack(padx=5, pady=5, fill=tk.BOTH, expand=True)   
   
        #error_mess = u"*** Requête SQL incorrecte ***\nVoici l'erreur retournée par sqlite : "+ e.args[0].decode('utf-8') + u"\nRectifiez votre requête :\n"
        error_mess = u"*** Requête SQL incorrecte ***\nVoici l'erreur retournée par sqlite : "+ e.args[0] + u"\nRectifiez votre requête :\n"
        
        fenetre.mess.config(text=error_mess)
        fenetre.txt.insert('1.0', requete)    
        
    else:
        fenetre.requete_frame.pack_forget()
        fenetre.results_frame.destroy()
        
        fenetre.mess.config(text=u'Saisissez votre requête : \n')
        fenetre.txt.delete('1.0', tk.END)        
        
        fenetre.results_frame = tk.LabelFrame(fenetre, text='', width=500, borderwidth=2,relief=tk.GROOVE, padx=5, pady=5)
        fenetre.results_frame.pack(padx=5, pady=5, fill=tk.BOTH, expand=True)

        #fenetre.log_file.write((u'\n\n%s\n' % (requete,)).encode('utf-8'))
#        fenetre.log_file.write(('\n\n%s\n' % (requete,)))
        
#        fenetre.log_file.flush()

        if 'SELECT'.lower() in requete.lower() :
           
#            affiche_select(cur,requete, fenetre.results_frame, fenetre.log_file)
            affiche_select(cur,requete, fenetre.results_frame)
            
        elif 'CREATE'.lower() in requete.lower() :
            #print 'requete de creation de la table :'
            requete_words = requete.split()
            table_name = requete_words[2]
            #print table_name
            mess = u'Votre requête a modifié le schéma de la base de données en ajoutant la table nommée : '
            mess+= table_name
            tkMessageBox.showinfo("",mess)
            clear_fenetre()
            compose_fenetre()
            activate_tab(table_name)

        elif 'DROP'.lower() in requete.lower() :
            #print 'requete de suppression de la table :'
            requete_words = requete.split()
            table_name = requete_words[2]
            #print table_name
            mess = u'Votre requête a modifié le schéma de la base de données en supprimant la table nommée : '
            mess+= table_name
            tkMessageBox.showinfo("",mess)
            clear_fenetre()
            compose_fenetre()
            
        elif 'ALTER'.lower() in requete.lower() : 
            #print 'requete de moficiation de la structure d une table'
            requete_words = requete.split()
            table_name = requete_words[2]
            #print table_name
            
            mess = u'Votre requête concernant la table '+ table_name + u' a bien été prise en compte.'
            #mess+= table_name
            tkMessageBox.showinfo("",mess)
            clear_fenetre()
            compose_fenetre()
            if 'rename' in requete.lower() : 
                table_name = requete_words[-1]
            activate_tab(table_name)
            
        else :
            
            
            
            requete_words = requete.split()
            if requete_words[1].lower() == 'INTO'.lower() or requete_words[1].lower() == 'FROM'.lower() :
                table = requete_words[2]
            else : #UPDATE
                if 'OR'.lower() in requete.lower() :
                    table = requete_words[3] 
                else :
                    table = requete_words[1] 
            if '(' in table :
                table = table.split('(')[0]
            #print table
            fenetre.modif_table = table   
               
            tk.Label(fenetre.results_frame, text= u"\nVotre requête "+requete+u"  a modifié le contenu de la table "+table,font = "Verdana 10").pack(anchor=tk.W)            
            
#            fenetre.log_file.write(u'Voici les effets de la modification : \n')
            
            
            requete = 'select * from '+table
            #print requete
            cur.execute(requete)
            
#            affiche_modifs(cur,requete, fenetre.results_frame, fenetre.log_file)
            affiche_modifs(cur,requete, fenetre.results_frame)
             
            tk.Label(fenetre.results_frame, text= u"Voulez-vous valider cette modification ?").pack(pady=8)
            fr_button = tk.Frame(fenetre.results_frame)
            fr_button.pack(side=tk.BOTTOM, padx=10, pady=10)                     
            tk.Button(fr_button, text=u"Valider", command=valider_modif).grid(row=0,column=0, padx=10)
            tk.Button(fr_button, text=u"Annuler", command=abandonner_modif).grid(row=0,column=1, padx=10)

    
    
def executer():
    requete = fenetre.txt.get('1.0', tk.END)
    #print '***'+requete+'***'
    requete = requete.replace(' \n',' ')
    requete = requete.replace('\n',' ')
    requete = requete.strip()
    fenetre.txt.delete('1.0',tk.END)
    #print  '***'+requete+'***'
    if requete!='' :
        if ('CREATE'.lower() in requete.lower()) or ('DROP'.lower() in requete.lower()) or ('ALTER'.lower() in requete.lower()) :
            #print 'requete de modification du schéma de BD'
            if tkMessageBox.askokcancel(title=u"Confirmation", message=u'Votre requête si elle est valide va modifier la structure de votre base de données.\n Souhaitez-vous poursuivre ?') :
                exec_req(requete)
            #else :
            #    print 'annulation'
        else :
            #print 'requete qui ne modifie pas la schema'
            exec_req(requete)


def activate_tab(table_name):
    for tab_id in fenetre.schema.box.tabs():
        tab_name = fenetre.schema.box.tab(tab_id)['text']
        if tab_name.lower() == table_name.lower() :
            fenetre.schema.box.select(tab_id)

def clear_fenetre():
    try :
        fenetre.open_frame.destroy()
    
        fenetre.schema.box.destroy()
        fenetre.requete_frame.destroy()
        fenetre.mess.destroy()
        fenetre.txt.destroy()
        fenetre.exec_button.destroy()
        fenetre.results_frame.destroy()
    except :
        pass

def compose_fenetre():
    clear_fenetre()
    fenetre.schema = bdGUI.schema_bd(fenetre.baseDonn,fenetre.baseName)
    fenetre.schema.GUI(fenetre.baseDonn, fenetre,['white','LemonChiffon3'])
    fenetre.schema.box.pack(fill=tk.X)
    #fenetre.schema.box.select(1)
    #print fenetre.schema.box.tabs()
    #print fenetre.schema.box.tab(fenetre.schema.box.select())['text']
    
    fenetre.requete_frame = tk.LabelFrame(fenetre, text='', borderwidth=2,relief=tk.GROOVE, padx=8, pady=8)
    fenetre.requete_frame.pack(padx=3, pady=3, fill=tk.BOTH, expand=True)
    
    
    fenetre.mess = tk.Label(fenetre.requete_frame, text=u"Saisissez votre requête :", font = "Verdana 11" , anchor=tk.W, justify=tk.LEFT)
    fenetre.mess.pack(anchor=tk.W)
    fenetre.txt = tk.Text(fenetre.requete_frame, height=10, width=80)#, wrap=tk.WORD)
    fenetre.txt.pack( fill=tk.BOTH, expand=True, padx=4, pady=10)#side=tk.LEFT,
    
    fenetre.exec_button = tk.Button(fenetre.requete_frame, text=u'Exécuter', command=executer)
    fenetre.exec_button.pack(side=tk.BOTTOM, padx=5, pady=5)#, expand=True)
    
    fenetre.results_frame = tk.LabelFrame(fenetre, text='', borderwidth=2,relief=tk.GROOVE, padx=5, pady=5)




#def exporter():
#    dataname = fenetre.schema.nom
#    directory = tkFileDialog.askdirectory()
#
#    export_filename = os.path.join(directory,dataname+'.sql')
#    export_file = open(export_filename,'w')
#    for table in fenetre.schema.tables :
#        #export du schema
#        pkeys = []        
#        requete = 'CREATE TABLE '
#        requete += table.nom + ' ('
#        for champ in table.champs[:-1] :
#            requete += champ.nom + ' ' + champ.type
#            if champ.pk :
#                pkeys.append(champ.nom)
#                
#            if champ.null :
#                requete += ' NOT NULL'
#            requete += ', '
#        champ = table.champs[-1]
#        requete += champ.nom + ' ' + champ.type
#        if champ.pk :
#            pkeys.append(champ.nom)
#            
#        if champ.null :
#            requete += ' NOT NULL'
#        #print pkeys
#        pk_text = ','.join(pkeys)
#
#        requete+= ', PRIMARY KEY(' + pk_text + ')' 
# 
#        for fk in table.fkeys :
#            requete+= ', FOREIGN KEY(' + fk.champ_source.nom + ') REFERENCES ' + fk.table_cible + '(' + fk.champ_cible + ')'
#        requete += ') ;'
#        export_file.write(requete+'\n\n')
#        
#        #export du contenu
#        requete='select * from '+table.nom
#
#        try:
#        
#            sav = fenetre.baseDonn.row_factory  # sauvegarde de l'ancienne valeur (None par défaut)
#            fenetre.baseDonn.row_factory = sqlite3.Row
#            cur = fenetre.baseDonn.cursor()
#            fenetre.baseDonn.row_factory = sav # retour à l'ancienne valeur
#            cur.execute(requete)
#
#
#        except :
#            print 'erreur'
#        else :
#            r = cur.fetchone()
#            if r : #Si on a des données correspondant à la requête
#                #on récupère les noms des colonnes
#                champs = r.keys()
#
#                for enreg in cur: 	
#                    values = []
#                    for val in enreg :
#                        values.append('"'+str(val)+'"')
#                    insert_requete = 'insert into '+ table.nom + ' (' + ','.join(champs)+') values ('+','.join(values)+');\n'
#                    export_file.write(insert_requete)
#
#    
#        export_file.write('\n\n')        
#    export_file.close()

#def importer():
#    filename = tkFileDialog.askopenfilename(title="Importer une base de données",filetypes=[('sqlite files','.sqlite'),('all files','.*')])
#    file_insert = open(filename, 'r')
#    lignes = file_insert.readlines()
#    file_insert.close()
#    
#    cur = fenetre.baseDonn.cursor()
#    for ligne in lignes :
#        print ligne
#        cur.execute(ligne)
#     
#    fenetre.baseDonn.commit()

def add_from_sqlFile() :
    filename = tkFileDialog.askopenfilename(title=u"Sélectionnez votre fichier de requêtes",filetypes=[('sql files','.sql'),('all files','.*')])
    print (filename)

    cur = fenetre.baseDonn.cursor()
    
    file_insert = open(filename, 'r')
    lignes = file_insert.readlines()
    file_insert.close()
    complet = True
    errors = []
    requete = ''
    for ligne in lignes :
        requete += ligne
        if ';' in ligne :
            print (requete)
            try :
                cur.execute(requete)
            except sqlite3.Error as e :
                complet = False
                errors.append([requete,e])
            requete = ''
    fenetre.baseDonn.commit()
    
    if not complet :
        dir_path = os.path.dirname(filename)
        log_path = os.path.join(dir_path,'log')
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        log_name = fenetre.baseName+'_insert_errors.txt'
        log_name = os.path.join(log_path,log_name)
        file_insert_errors = open(log_name, 'w')         

        file_insert_errors.write('Liste des requêtes et erreurs associées depuis le fichier :\n')
        file_insert_errors.write(filename+'\n\n')        
        for requete, error in errors :

            file_insert_errors.write('----\n') 
            file_insert_errors.write(requete+'\n') 
#            file_insert_errors.write(unicode(error)+'\n')
            file_insert_errors.write(error+'\n')            
            file_insert_errors.write('----\n\n\n') 



        mess = u'Une ou plusieurs de vos requêtes ont déclenché des erreurs \n'
        mess += "et n'ont de ce fait pas été prises en compte.\n\n"
        mess += 'Pour plus de détails reportez-vous au fichier de log nommé :\n'
        mess += log_name+'\n\n'
        mess += 'Vos requêtes valides ont bien été exécutées !'
        tkMessageBox.showinfo("",mess)  
    else :
        mess = u'Toutes vos modifications sont prises en compte !'
        tkMessageBox.showinfo("",mess)
     

    clear_fenetre()
    compose_fenetre()

    

def open_bd(filename) :

    basename = os.path.basename(filename) #filename.split('/')[-1]
    #print basename
    #dir_path = filename.split('/')[:-1]
#    dir_path = os.path.dirname(filename)
    #print dir_path
#    log_path = os.path.join(dir_path,'log')
    if basename !='' :
        fenetre.baseDonn = sqlite3.connect(filename)
        fenetre.baseName = basename.split('.')[0]
        cur = fenetre.baseDonn.cursor()
        cur.execute('PRAGMA foreign_keys = ON')
        #mise à jour du menu
        menufichier.delete(0)
        menufichier.delete(0)
        menufichier.insert_command(0,label=u"Fermer la base de données",command=Fermer)
        menufichier.insert_command(0,label=u"Ajouter des données à partir d'un fichier de requêtes",command=add_from_sqlFile)
        #menufichier.insert_command(0,label="Exporter la base de données",command=exporter)
        #menufichier.insert_command(0,label="Importer dans la base de données",command=importer)
        
#        if not os.path.exists(log_path):
#            os.makedirs(log_path)
        
#        log_name = time.strftime('log_file_%Y_%m_%d_'+basename.split('.')[0]+'.txt',time.localtime())
#        log_name = os.path.join(log_path,log_name)
#        fenetre.log_file = open(log_name, 'a') 
        
#        if (file_is_empty(log_name)) :
#            date = time.strftime(u'%d/%m/%Y',time.localtime()) 
#            entete = "Le : "+date+u"\n"
#            entete += "Base de données : "+basename+'\n'
#            entete += "Requêtes valides effectuées et résultats correspondant\n"
#            #entete = entete.encode('utf8')
#            entete = entete
            
#            fenetre.log_file.write(entete)
            
            #fenetre.log_file.write(u"Base de données : "+basename+u'\n')
            #fenetre.log_file.write(u"Requêtes valides effectuées et résultats correspondant\n")
#            fenetre.log_file.flush() 

        if not file_is_empty(filename) :    
            fenetre.title_frame = tk.LabelFrame(fenetre,text='', padx=8, pady=8, borderwidth=2,relief=tk.GROOVE)
            fenetre.title_frame.pack(padx=3, pady=3, fill=tk.X)
            tk.Label(fenetre.title_frame, text=u"Vous travaillez sur la base de données "+basename.split('.')[0], font = "Verdana 13 bold" ).pack()#, background='LemonChiffon3'   
            #tk.Label(fenetre, text="Vous travaillez sur la base de données "+basename.split('.')[0], font = "Verdana 13 bold" ).pack()#, background='LemonChiffon3'   
            compose_fenetre()
            
        else :
            tkMessageBox.showinfo(u"",u"La base de données sélectionnée est vide")
            fenetre.title_frame = tk.LabelFrame(fenetre,text='', padx=8, pady=8, borderwidth=2,relief=tk.GROOVE)
            fenetre.title_frame.pack(padx=3, pady=3, fill=tk.X)
            tk.Label(fenetre.title_frame, text=u"Vous travaillez sur la base de données "+basename.split('.')[0], font = "Verdana 13 bold" ).pack()#, background='LemonChiffon3'   
  
            compose_fenetre()
    else :
        clear_fenetre()
        tkMessageBox.showinfo(u"",u"Vous n'avez sélectionné aucune base de données")
    
  
def Ouvrir():    

    filename = tkFileDialog.askopenfilename(title=u"Se connecter à une base de données",filetypes=[('sqlite files','.sqlite'),('all files','.*')])
    fenetre.open_frame = tk.LabelFrame(fenetre,text='', padx=8, pady=8, borderwidth=2,relief=tk.GROOVE)
    tk.Label(fenetre.open_frame, text=u"Votre base de données est en cours d'ouverture").pack(anchor=tk.CENTER, fill=tk.BOTH, expand=True)
    #pb = ttk.Progressbar(fenetre.open_frame, orient='horizontal', mode='indeterminate')
    #pb.pack(expand=True, fill=tk.BOTH, side=tk.TOP)
    #pb.start()
    fenetre.open_frame.pack(anchor=tk.CENTER,fill=tk.BOTH, expand=True)
    fenetre.update()
    #time.sleep(5)
    open_bd(filename)


def Fermer():
    #print "fermer"
    if tkMessageBox.askokcancel(title=u"Confirmation", message=u'Voulez-vous quitter la base de données : ') :
        children = fenetre.winfo_children()
        for child in children[1:] :
            child.destroy()
        menufichier.delete(0)
        menufichier.delete(0)
        #menufichier.delete(0)
        menufichier.insert_command(0,label=u"Créer une base de données",command=Creer)
        menufichier.insert_command(0,label=u"Se connecter à une base de données",command=Ouvrir)
        fenetre.baseDonn.close()
#        fenetre.log_file.close()

def Creer():
    filename = tkFileDialog.asksaveasfilename(title=u"Créer une nouvelle base de données",filetypes=[(u'sqlite files',u'.sqlite'),(u'all files','.*')],defaultextension=u'.sqlite')
    print (filename)    
    bd_file = open(filename, 'w')
    bd_file.close()
    open_bd(filename) 
    
        
def quitter():
    #print "quitter"
    if tkMessageBox.askokcancel(title=u"Confirmation", message=u"Voulez-vous quitter l'application : ") :
        try :        
            fenetre.baseDonn.close()
#            fenetre.log_file.close()
        except :
            fenetre.destroy()
        else :
            fenetre.destroy()

def Apropos():
    tkMessageBox.showinfo(u"A propos",u"Application développée pour les élèves d'AgroParisTech\n(C) Christine Martin\n version 2.0\n 2018")


def create_menu():
    pass


    
#------------------------------------------------------------------------------
#             Programme principal
#------------------------------------------------------------------------------





fenetre = tk.Tk()
#fenetre.attributes("-fullscreen", 1)
fenetre.title(u"Explorateur de bases de données SQLite")

fenetre['bg']='LemonChiffon3' # couleur de fond 
#fenetre.configure(width=1500,height=1000)
screen_height = str(fenetre.winfo_screenheight()-100)
screen_width = str(fenetre.winfo_screenwidth()-25)
fenetre.geometry(screen_width+"x"+ screen_height+"+5+5")
#fenetre.geometry("1200x650+5+5")
fenetre.minsize(width=1000, height=666)
#fenetre.geometry("+5+5")
#fenetre.resizable(width=False,height=False)

# Création d'un widget Menu
menubar = tk.Menu(fenetre)

menufichier = tk.Menu(menubar,tearoff=0)
menufichier.add_command(label=u"Se connecter à une base de données",command=Ouvrir)
menufichier.add_command(label=u"Créer une base de données",command=Creer)
menufichier.add_command(label=u"Quitter",command=quitter)
menubar.add_cascade(label=u"Fichier", menu=menufichier)

menuaide = tk.Menu(menubar,tearoff=0)
menuaide.add_command(label=u"A propos",command=Apropos)
menubar.add_cascade(label=u"Aide", menu=menuaide)

# Affichage du menu
fenetre.config(menu=menubar)

#print fenetre.winfo_children()


fenetre.mainloop()


